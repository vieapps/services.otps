﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: ComVisible(false)]

[assembly: AssemblyTitle("VIEApps NGX OTPs")]
[assembly: AssemblyCompany("VIEApps.net")]
[assembly: AssemblyProduct("VIEApps NGX")]
[assembly: AssemblyCopyright("© 2021 VIEApps.net")]

[assembly: AssemblyVersion("10.3.2007.1")]
[assembly: AssemblyFileVersion("10.3.2007.1")]
[assembly: AssemblyInformationalVersion("10.3.2020.7.5@net48#let.it.be")]
